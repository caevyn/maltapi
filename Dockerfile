# Dockerfile for test
FROM microsoft/aspnetcore-build:2.0
WORKDIR /app
RUN apt-get -q update && apt-get -qy install netcat
# Copy csproj and restore as distinct layers
COPY Malt.sln ./
COPY wait-for ./
COPY MaltApi.Test/ ./MaltApi.Test/
COPY MaltApi/ ./MaltApi/
RUN dotnet restore
CMD sh -c './wait-for db:5432' && \
    cd MaltApi && \
    dotnet ef database drop -f && \
    dotnet ef database update && \
    dotnet test ../MaltApi.Test/MaltApi.Test.csproj