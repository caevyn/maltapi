local _M = {}

local per_worker_data = {}

function _M.get_data()
  if per_worker_data["data"] == nil then
    per_worker_data["data"] = "first loaded at " .. ngx.var.time_iso8601
  end
  return per_worker_data["data"]
end

return _M
