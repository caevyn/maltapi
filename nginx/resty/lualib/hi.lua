local _M = {}

-- this is shared across requests
local default_name = ""

function _M.get(name)
  name = name or default_name
  return "hi " .. name
end

return _M
