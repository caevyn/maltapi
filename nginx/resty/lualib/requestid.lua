local _M = {}

function _M.get()
  local req_id = ngx.var.http_correlationId
  if(req_id == nil) then
    local resty_random = require "resty.random"
    local str = require "resty.string"
    local random = resty_random.bytes(11)
    req_id = str.to_hex(random)
  end
  return req_id
end

return _M
