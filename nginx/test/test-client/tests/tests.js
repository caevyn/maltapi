'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const request  = require('superagent');
const assert   = require('assert');

console.log('running tests...');

describe('router', function () {

  this.timeout(10000);

  it('fake-backend.example.com to fake-backend.test', done => {

    request
      .get('http://nginx:8080/proxy')
      .set('host', 'fake-backend.example.com')
      .redirects(false)
      .end((err, res) => {
        assert(!err, `error not expected but was ${JSON.stringify(err)}`);
        assert.equal(res.status, 200);
        let info = JSON.parse(res.text);
        assert.equal(info.headers['host'], 'fake-backend.test');
        done();
      });

  });

  it('no host header routes to catch all backend', done => {

    request
      .get('http://nginx:8080/proxy')
      .set('host', 'my-nib.kaos.nibit.com.au')
      .redirects(false)
      .end((err, res) => {
        assert(!err, `error not expected but was ${JSON.stringify(err)}`);
        assert.equal(res.status, 200);
        let info = JSON.parse(res.text);
        assert.equal(info.backend, 'match all fake');
        done();
      });

  });

  it('removes headers that identify the server', done => {

    request
      .get('http://nginx:8080/')
      .set('host', 'my.nib-cf-test.com')
      .end((err, res) => {
        assert(!err, `error not expected but was ${JSON.stringify(err)}`);
        assert.equal(res.header['server'], 'cats');
        assert.equal(res.header['x-powered-by'], null);
        done();
      });

  });


});
