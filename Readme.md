## Malt Api

This is just a repo I created over the weekend to familiarise myself with dotnet core 2.
It is not something I intended to show anyone, but might show some thought process around how I approach learning a new tech.

It is just a simple crud api, the tests are mostly there to try out the MVC test server not a great indication of the sort of tests I'd write on production code.

There are a couple of DB libraries I was looking at, Entity Framework, which I've never used with .net and possibly would still prefer something simple like Dapper. EF has improved a lot since I last looked though.

I also have tried a library called Marten which is a nice DocDb/Event library for postgres.

```sh
docker-compose up
```

Gets you a bit of a dev stack I was playing with. Nginx -> mvc -> postgres with postgres admin. The nginx container is a left over from a talk I did at the infracoders meetup last year about openresty which is still at http://localhost:8080/slides
http://localhost:8080/api/malt proxies the api.

```sh
docker-compose -f docker-compose-tests.yml up --abort-on-container-exit
```

runs the tests in a container with a clean postgres with ef migrations, could use something similar in a build pipeline.

Marten/Entity Framework can be toggled with the UseDocumentDB Env variable. To run with marten use:

```sh
docker-compose -f docker-compose-tests-docdb.yml up --abort-on-container-exit
```