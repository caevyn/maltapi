using AspNetCore.Http.Extensions;
using MaltApi.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace MaltApi.Test
{
    public class MaltApiTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public MaltApiTests()
        {
            var connectionString = Environment.GetEnvironmentVariable("ASPNETCORE_ConnectionStrings__Postgres");
            if(string.IsNullOrEmpty(connectionString))
            {
                Environment.SetEnvironmentVariable("ASPNETCORE_ConnectionStrings__Postgres", "Server=localhost;Database=malt;Username=postgres;Password=password");
            }
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task Get_by_id_Should_404_if_not_found()
        {
            var response = await _client.GetAsync("/api/malt/999");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Get_by_id_Should_return_malt_record()
        {
            var postResponse = await _client.PostAsJsonAsync<Malt>("/api/malt", new Malt{Name="test", Country="Germany", ColorEbc=5, Maltster="Weyerman"});
            var created = await postResponse.Content.ReadAsJsonAsync<Malt>();
            var response = await _client.GetAsync($"/api/malt/{created.Id}");
            response.EnsureSuccessStatusCode();
            var malt = await response.Content.ReadAsJsonAsync<Malt>();
            Assert.Equal(created.Id, malt.Id);
        }

        [Fact]
        public async Task Post_Should_create_new_malt_record()
        {
            var response = await _client.PostAsJsonAsync("/api/malt", new Malt{Grain="barley", ColorEbc=32, Country="Germany", Name="CaraAroma", Maltster="Weyerman"});
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            var malt = await response.Content.ReadAsJsonAsync<Malt>();
            Assert.True(malt.Id != Guid.Empty);
            Assert.Equal("Germany", malt.Country);
        }

        [Fact]
        public async Task Put_Should_update_existing_malt_record()
        {
            var postResponse = await _client.PostAsJsonAsync<Malt>("/api/malt", new Malt{Name="test", Country="Germany", ColorEbc=5, Maltster="Weyerman"});
            var created = await postResponse.Content.ReadAsJsonAsync<Malt>();
            var response = await _client.PutAsJsonAsync($"/api/malt/{created.Id}", new Malt{Id=created.Id, Grain="wheat", ColorEbc=32, Country="Germany", Name="CaraAroma", Maltster="Weyerman"});
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Put_Should_404_if_not_found()
        {
            var response = await _client.PutAsJsonAsync($"/api/malt/999", new Malt{Id=Guid.NewGuid(), Grain="wheat", ColorEbc=32, Country="Germany", Name="CaraAroma", Maltster="Weyerman"});
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Delete_Should_404_if_not_found()
        {
            var response = await _client.DeleteAsync($"/api/malt/{Guid.NewGuid()}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Delete_Should_Delete_Record()
        {
            var response = await _client.PostAsJsonAsync("/api/malt", new Malt{Grain="barley", ColorEbc=32, Country="Germany", Name="CaraBohemian", Maltster="Weyerman"});
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            var location = response.Headers.GetValues("location").First();
            var deleteResponse = await _client.DeleteAsync(location);
            response.EnsureSuccessStatusCode();
            var getResponse = await _client.GetAsync(location);
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }
    }
}
