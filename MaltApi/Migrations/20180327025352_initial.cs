﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MaltApi.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Malts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ColorEbc = table.Column<decimal>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    DiasticPower = table.Column<decimal>(nullable: false),
                    Grain = table.Column<string>(nullable: true),
                    Maltster = table.Column<string>(nullable: false),
                    Moisture = table.Column<decimal>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Protein = table.Column<decimal>(nullable: false),
                    Yield = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Malts", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Malts");
        }
    }
}
