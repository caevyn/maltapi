using Microsoft.EntityFrameworkCore;

namespace MaltApi.Models
{
    public class MaltDbContext : DbContext
    {
        public MaltDbContext(DbContextOptions<MaltDbContext> options) : base(options)
        {
        }
        public DbSet<Malt> Malts { get; set; }

    }
}