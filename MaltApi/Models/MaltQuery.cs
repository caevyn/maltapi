namespace MaltApi.Models
{
    public class MaltQuery
    {
        public int PageSize {get; set;}
        public int Page { get; set; }
    }
}