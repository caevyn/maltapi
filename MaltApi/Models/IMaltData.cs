using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MaltApi.Models
{

    public interface IMaltData
    {
        Task<Malt> Create(Malt malt);
        Task<Malt> Update(Malt malt);
        Task<Malt> Get(Guid id);
        Task<IEnumerable<Malt>> Get(MaltQuery query);
        Task<Guid> Delete(Guid id);
    }
}