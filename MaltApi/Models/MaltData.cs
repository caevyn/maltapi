using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MaltApi.Models
{
    public class MaltData : IMaltData
    {
        private readonly MaltDbContext _context;

        public MaltData(MaltDbContext context)
        {
            _context = context;
        }

        public async Task<Malt> Create(Malt malt)
        {
            var newMalt = _context.Malts.Add(malt);
            await _context.SaveChangesAsync();
            return newMalt.Entity;
        }

        public async Task<Guid> Delete(Guid id)
        {
            var malt = new Malt { Id = id };
            _context.Remove(malt);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch(DbUpdateConcurrencyException)
            {
                return Guid.Empty;
            }
            return id;
        }

        public async Task<Malt> Get(Guid id)
        {
            var malt = await _context.Malts.SingleOrDefaultAsync(m => m.Id == id);
            return malt;
        }

        public async Task<IEnumerable<Malt>> Get(MaltQuery query)
        {
            var malts = await _context.Malts.Skip(query.PageSize * (query.Page - 1)).Take(query.PageSize).ToListAsync();
            return malts;
        }

        public async Task<Malt> Update(Malt malt)
        {
            var existing = await _context.Malts.SingleOrDefaultAsync(m => m.Id == malt.Id);
            if(existing == null)
            {
                return null;
            }
            _context.Entry(existing).CurrentValues.SetValues(malt);
            await _context.SaveChangesAsync();
            return existing;
        }
    }
}