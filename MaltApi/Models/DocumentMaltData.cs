using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marten;
using Marten.Services;

namespace MaltApi.Models
{
    public class DocumentMaltData : IMaltData
    {
        private readonly IDocumentSession _session;

        public DocumentMaltData(IDocumentSession session)
        {
            _session = session;
        }

        public async Task<Malt> Create(Malt malt)
        {
            _session.Insert(malt);
            await _session.SaveChangesAsync();
            return malt;
        }

        public async Task<Guid> Delete(Guid id)
        {
            try
            {
                _session.Delete(id);
                await _session.SaveChangesAsync();
            } catch(NonExistentDocumentException)
            {
                return Guid.Empty;
            }
            return id;
        }

        public async Task<Malt> Get(Guid id)
        {
            var malt = _session.LoadAsync<Malt>(id);
            return await malt;
        }

        public Task<IEnumerable<Malt>> Get(MaltQuery query)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Malt> Update(Malt malt)
        {
            try{
              _session.Update(malt);
              await _session.SaveChangesAsync();
            } catch(NonExistentDocumentException)
            {
                return null;
            }
            return malt;
        }
    }
}