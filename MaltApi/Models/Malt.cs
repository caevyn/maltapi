﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MaltApi.Models
{
    public class Malt
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string Maltster { get; set; }
        public string Grain { get; set; }
        [Range(0, 1600)]
        public decimal ColorEbc { get; set; }
        public decimal Yield { get; set; }
        public decimal Protein { get; set; }
        public decimal Moisture { get; set; }
        public decimal DiasticPower { get; set; }
    }
}
