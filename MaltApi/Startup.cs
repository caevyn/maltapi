﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaltApi.Models;
using Marten;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MaltApi
{
    public class Startup
    {
        private readonly ILogger<Startup> _logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            var connectionString = Configuration.GetConnectionString("postgres");
            _logger.LogInformation($"using constring: {connectionString}");
            services.AddCors();
            var useDocDb = Configuration.GetValue<bool>("UseDocumentDB", false);
            if(useDocDb)
            {
                services.AddScoped<IMaltData, DocumentMaltData>();
                services.AddSingleton<IDocumentStore>(ctx => {
                    return DocumentStore.For(d => d.Connection(connectionString));
                });
                services.AddTransient(ctx => ctx.GetService<IDocumentStore>().LightweightSession());
            }
            else
            {
                services.AddScoped<IMaltData, MaltData>();
                services.AddDbContext<MaltDbContext>(options => options.UseNpgsql(connectionString));
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //simple correlation middleware
            app.Use(async (context, next) => {
                var correlationId = context.Request.Headers["x-correlation-id"].FirstOrDefault()?.ToString() ?? Guid.NewGuid().ToString("D");
                context.Response.OnStarting(() => {
                    context.Response.Headers.Add("x-correlation-id", correlationId);
                    return Task.CompletedTask;
                });
                await next.Invoke();
            });
            app.UseMvc();
        }
    }
}
