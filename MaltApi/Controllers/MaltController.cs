﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaltApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MaltApi.Controllers
{
    [Route("api/[controller]")]
    public class MaltController : Controller
    {
        private readonly IMaltData _maltData;
        private readonly ILogger<MaltController> _logger;

        public MaltController(IMaltData data, ILogger<MaltController> logger)
        {
            _maltData = data;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] MaltQuery query)
        {
            var result = await _maltData.Get(query);
            return Json(result);
            //return Json(Environment.GetEnvironmentVariables());
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _maltData.Get(id);
            _logger.LogInformation($"Logging for GET /api/malt/{id}");
            if(result == null)
            {
                return NotFound();
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Malt malt)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _maltData.Create(malt);
            return Created($"/api/malt/{result.Id}", result);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Malt malt)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await _maltData.Update(malt);
            if(result == null)
            {
                return NotFound();
            }
            return Json(result);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _maltData.Delete(id);
            if(result == Guid.Empty)
            {
                return NotFound();
            }
            return Json(result);
        }
    }
}
